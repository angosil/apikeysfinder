import pandas as pd
import os
from open_read import bom_type


# 1 Load the file 'GG - Analysis of sites to remove.xlsx'
# 2 Load the sheet 'Sites Cleanse Tracking'
sites_to_remove_xlsx = 'GG - Analysis of sites to remove.xlsx'
sites_df = pd.read_excel(sites_to_remove_xlsx, sheetname='Sites Cleanse Tracking')
# sites_df.info()

# 3 in file loaded filter al rows by column "Lote"=3 and get the "Api Key" column value
print()
print('----|filter al rows by column "Lote"=3 and get the "Api Key" column value|----')
filter_sites_df = sites_df['Api Key'][sites_df['Lote'] == 3]
print(filter_sites_df)

# 4 find this value in in all files in all project
print()
print('----|find this value in in all files in all project|----')


def list_files(folder_path: str):
    """
    list of files in path
    :type folder_path: str
    """
    folder = os.walk(folder_path)
    for root, dirs, files in folder:
        for file_name in files:
            local_file = '{0}/{1}'.format(root, file_name)
            if exclude_file(local_file, exclude_criteria):
                # print('file path: {0}'.format(local_file))
                for key in filter_sites_df:
                    find_key(key, local_file)


def exclude_file(file_name, criteria: list):
    """
    retur boolean if the file is accepted or not 
    :param file_name: 
    :type criteria: list
    :rtype: bool
    """
    file_pass = True
    for str_clriteria in criteria:
        if file_name.find(str_clriteria) != -1:
            file_pass = False
    return file_pass


def find_key(key, file_path):
    """

    :type file_path: str
    :type key: str
    :rtype: str
    """
    bom = bom_type(file_path)
    if key in open(file_path, encoding=bom, errors='ignore').read():
        print('found {0} in file {1}'.format(key, file_path))


projec_list = [
    'comentarios_api_intermedia',
    'gigyaexport',
    'gigya_symfony',
    'scriptsandtools',
    'userplayground',
    'user_lead',
    'vocento_gigya'
]

exclude_criteria = [
        '.gitkeep',
        '/.git/',
        '.htaccess',
        '.png',
        '.md',
        '.ico',
        '.jar',
        '.css',
        '.gif',
        '.jpg',
        '.woff',
        '.ttf',
        '.svg',
        '.eot',
        '.less',
        '.xlf',
        '.gitignore',
        'composer.',
        '.dist',
        '/.idea/',
        '.gdf',
        '/vendor/'
    ]

for path in projec_list:
    project_path = './../{0}'.format(path)
    print(project_path)
    list_files(project_path)
